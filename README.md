# Spray Net microsite static html

### Install Gulp and dependencies

```npm install gulp```

```npm install```

### Build project using Gulp

```gulp```


# To Do


## Changelogs Integration nedded
- Change font-size of hero text (need to add 300 weight to google font stylesheet) (commit: feccfde273106c7ddc3c88e57749da40f944e0cc)
- Block texte a gauche (Need to remove u-text-center classes on home and services blocks) (commit: aee2852cf1f079ee6d68827c9e5838f990ce19fc)
- Remove Reviews and add home to nav (Need to update header and footer html) (commit: b8e42a8ed7f72bbf809105fec488222edce9c981)
- Phone field placeholder uppercase (Need to change the letter) (commit: dca749b887685b88490965882349390034127b62)
- Missing All icons (Need to change HTML for each icon) (commit: eeda6f2fbe5e8ec5238755a242cb62fb869ec35c)
	- Menu toggle
	- Menu phone
	- Hero down arrow
	- Filter dropdown arrows
	- Careers right arrow
	- Transformation filter close
	- Transformation previous next arrows
	- Footer contact us section and page
	- Footer socials


## Changelogs No integration needed
- Hero text is more centered (nothing to integrate, just css)
- Font base 16 (Nothing to integrate, just css)
- Add border to transformation filters #999999 (nothing to integrate, just css)


## Known bugs and todos in static html
- Missing All icons
	- Menu toggle
	- Menu phone
	- Hero down arrow
	- Filter dropdown arrows
	- Careers right arrow
	- Transformation filter close
	- Footer socials
