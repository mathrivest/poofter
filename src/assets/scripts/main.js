$(function() {

    $('.Carousel').slick({
        dots: true,
        infinite: false,
        slidesToShow: 2,
        slidesToScroll: 2,
        speed: 400
    });

    $('.js-toggle-filters').on('click', function(){
        toggleFilters();
        return false;
    });

    $('.Transformations-item-slider').slick({
        arrows: true,
        dots: true,
        infinite: false,
        slidesToShow: 1,
        speed: 200,
        prevArrow: '<button type="button" class="slick-prev">Before</button>',
        nextArrow: '<button type="button" class="slick-next">Now</button>'
    });

    $('.js-navTrigger').on('click', function(){
        toggleNav();
        return false;
    });

    $('.js-appointment-form').hide();
    $(".js-appointment-trigger-form").submit(function(e){
        e.preventDefault();
        var pc = $(this).find('.js-appointment-trigger-pc').val();
        scrollToAppointmentForm(pc);
        return false;
    });

});

function toggleFilters() {
    var $filters = $('.js-filters');
    if($filters.is(':visible')) {
        $filters.addClass('u-hidden-sm-down');
        $('.TransformationsFilters-trigger').removeClass('u-hidden-sm-down');
    } else {
        $filters.removeClass('u-hidden-sm-down');
        $('.TransformationsFilters-trigger').addClass('u-hidden-sm-down');
    }
}

function toggleNav() {
    $('.js-nav').toggleClass('is-open');
}

function scrollToAppointmentForm(pc) {
    $appointmentForm = $('.js-appointment-form');

    $appointmentForm.show();
    $appointmentForm.find('.js-appointment-pc').val(pc);

    $('html, body').animate({scrollTop: $appointmentForm.offset().top},'slow');
}